from matplotlib.colors import LinearSegmentedColormap
import matplotlib.pyplot as plt
import numpy as np
from obspy import UTCDateTime
from obspy.signal.util import next_pow_2
import os
from scipy.signal import spectrogram


def cmp_spectro(trace, windows_duration, overlap_percentage):
    nfft = next_pow_2(trace.stats.npts / 13)
    nwin = int(windows_duration*trace.stats.sampling_rate)
    nover = int(nwin/overlap_percentage)       # number of overlapping samples
    return spectrogram(trace.data, trace.stats.sampling_rate, nfft=nfft,
                       window='hann', nperseg=nwin, noverlap=nover, mode='psd')


def roundday(datetime, method='closest'):
    time = UTCDateTime(datetime)
    if time.hour >= 12 and method == 'closest':
        return UTCDateTime(year=time.year, julday=time.julday+1)
    else:
        return UTCDateTime(year=time.year, julday=time.julday)


def spectro(trace, C1, C2, colmap):
    """
    Compute spectrograms
    -----------------------------------------
    INPUT :
    trace : input data
    C1     : [cmin, cmax] for the colorbar of the high frequency spectro
    C2     : [cmin, cmax] for the colorbar of the low frequency spectro
    colmap : colormap used for plotting
    typ    : seed sampling letter (B, H or L)
    """

    # -------------------------------------------------------------------------
    # FIGURE PREPARATION
    # -------------------------------------------------------------------------
    fig = plt.figure(dpi=300, figsize=(11.693, 8.268))
    ax1 = plt.subplot2grid((6, 40), (0, 0), rowspan=2, colspan=39, fig=fig)
    ax2 = plt.subplot2grid((6, 40), (0, 39), rowspan=2, colspan=1, fig=fig)
    ax3 = plt.subplot2grid((6, 40), (2, 0), rowspan=1, colspan=39, fig=fig)
    ax4 = plt.subplot2grid((6, 40), (3, 0), rowspan=2, colspan=39, fig=fig)
    ax5 = plt.subplot2grid((6, 40), (3, 39), rowspan=2, colspan=1, fig=fig)
    ax6 = plt.subplot2grid((6, 40), (5, 0), rowspan=1, colspan=39, fig=fig)

    # -------------------------------------------------------------------------
    # COMPUTE AND PLOT HIGH FREQUENCIES SPECTRO [0 - 10 Hz]
    # -------------------------------------------------------------------------
    toffset = trace.stats.starttime - roundday(trace.stats.starttime)
    f, t, Sxx = cmp_spectro(trace, 60, 10)
    _qm = ax1.pcolormesh(toffset+t, f, 10*np.log10(abs(Sxx)), cmap=colmap,
                         shading='nearest', vmin=C1[0], vmax=C1[1])
    ax1.set_ylabel('f [Hz]')
    ax1.axis([0, 8.634e+004, 0, 10])
    ax1.set_xticks(np.arange(0, 86500, 3600))
    ax1.set_xticklabels([])
    fig.colorbar(mappable=_qm, cax=ax2, label="[dB][m^2/s^2/Hz]")

    # -------------------------------------------------------------------------
    # PLOT HIGH FREQUENCIES TRACE [1 - 90% Nyquist Hz]
    # -------------------------------------------------------------------------
    tr_high = trace.copy()
    tr_high.filter('highpass', freq=1., zerophase=True, corners=2)
    time = np.arange(toffset, toffset + tr_high.stats.npts*tr_high.stats.delta,
                     tr_high.stats.delta)

    ax3.plot(time, tr_high.data, 'k')
    ax3.set_ylabel('Vel. [m/s]')
    ax3.axis([0, 8.634e+004,
              -10*(np.std(tr_high.data)), 10*(np.std(tr_high.data))])
    ax3.set_xticks(np.arange(0, 86500, 3600))
    ax3.set_xticklabels([])
    ax3.ticklabel_format(axis='y', style='scientific',
                         useOffset=False, scilimits=(2, -2))

    # -------------------------------------------------------------------------
    # PLOT LOW FREQUENCIES SPECTRO [0 - 1 Hz]
    # -------------------------------------------------------------------------
    tr_low = trace.copy()
    tr_low.resample(2)
    f, t, Sxx = cmp_spectro(tr_low, 600, 50)
    _qm = ax4.pcolormesh(toffset+t, f, 10*np.log10(abs(Sxx)), cmap=colmap,
                         shading='nearest', vmin=C2[0], vmax=C2[1])
    ax4.set_ylabel('f [Hz]')
    ax4.set_yscale('log')
    ax4.axis([0, 8.634e+004, 0.001, 1])
    ax4.set_xticks(np.arange(0, 86500, 3600))
    ax4.set_xticklabels([])
    fig.colorbar(mappable=_qm, cax=ax5, label="[dB][m^2/s^2/Hz]")

    # -------------------------------------------------------------------------
    # PLOT LOW FREQUENCIES TRACES [0.01 - 1 Hz]
    # -------------------------------------------------------------------------
    tr_low.filter('highpass', freq=0.005, zerophase=True, corners=2)
    time = np.arange(toffset, toffset + tr_low.stats.npts*tr_low.stats.delta,
                     tr_low.stats.delta)

    ax6.plot(time, tr_low.data, 'k')
    ax6.set_xlabel('Time [h]')
    ax6.set_ylabel('Vel. [m/s]')
    ax6.axis([0, 8.634e+004,
              -10*(np.std(tr_low.data)), 10*(np.std(tr_low.data))])
    ax6.set_xticks(np.arange(0, 86500, 3600))
    ax6.set_xticklabels(['0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                         '10', '11', '12', '13', '14', '15', '16', '17', '18',
                         '19', '20', '21', '22', '23', '24'])
    ax6.ticklabel_format(axis='y', style='scientific',
                         useOffset=False, scilimits=(2, -2))

    tid = roundday(trace.stats.starttime)
    filename = "{}.{}.{}.{}.{}.{:03d}".format(trace.stats.network,
                                              trace.stats.station,
                                              trace.stats.location,
                                              trace.stats.channel,
                                              tid.year,
                                              tid.julday)
    filepath = os.path.join("Figures", "{}".format(tid.year),
                            "{}".format(trace.stats.network),
                            "{}".format(trace.stats.station),
                            "{}".format(trace.stats.channel))
    fig.suptitle(filename)
    if not os.path.exists(filepath):
        os.makedirs(filepath, exist_ok=True)
    fileout = os.path.join(filepath, "{}.jpg".format(filename))
    plt.savefig(fileout)


def create_custom_colormap():
    colors = [ (     0,      0,      0),
               (0.0100,      0, 0.0200),
               (0.0200,      0, 0.0400),
               (0.0300,      0, 0.0600),
               (0.0400,      0, 0.0800),
               (0.0500,      0, 0.1000),
               (0.0600,      0, 0.1200),
               (0.0700,      0, 0.1400),
               (0.0800,      0, 0.1600),
               (0.0900,      0, 0.1800),
               (0.1000,      0, 0.2000),
               (0.1100,      0, 0.2200),
               (0.1200,      0, 0.2400),
               (0.1300,      0, 0.2600),
               (0.1400,      0, 0.2800),
               (0.1500,      0, 0.3000),
               (0.1600,      0, 0.3200),
               (0.1700,      0, 0.3400),
               (0.1800,      0, 0.3600),
               (0.1900,      0, 0.3800),
               (0.2000,      0, 0.4000),
               (0.1857,      0, 0.4429),
               (0.1714,      0, 0.4857),
               (0.1524,      0, 0.5429),
               (0.1333,      0, 0.6000),
               (0.0667,      0, 0.8000),
               (     0,      0, 1.0000),
               (     0, 0.2510, 1.0000),
               (     0, 0.5020, 1.0000),
               (     0, 0.7510, 0.5000),
               (     0, 1.0000,      0),
               (0.5000, 1.0000, 0.3451),
               (1.0000, 1.0000, 0.6902),
               (1.0000, 1.0000, 0.5666),
               (1.0000, 1.0000, 0.4431),
               (1.0000, 0.8206, 0.4029),
               (1.0000, 0.6412, 0.3627),
               (1.0000, 0.4618, 0.3226),
               (1.0000, 0.2824, 0.2824),
               (1.0000, 0.2353, 0.2353),
               (1.0000, 0.1883, 0.1883),
               (1.0000, 0.1412, 0.1412),
               (1.0000, 0.0941, 0.0941),
               (1.0000, 0.0471, 0.0471),
               (1.0000,      0,      0),
               (0.9007,      0,      0),
               (0.8013,      0,      0),
               (0.7020,      0,      0)]
    colmap = LinearSegmentedColormap.from_list('specmap', colors, N=48)
    return colmap
