# sismo-data-mining

## Description
Multi-processing spectrogram calculation and visualization tool for seismic data, as used by Zigone and Lambotte.  

## Installation
See requirements.txt and install it if needed:  
``` python3 -m pip install -r requirements.txt ```  
The usage of a virtual environment is a must.  

## Usage
For basic help, run:  
``` python3 run_spectro_webs.py -h```

## Example
``` python3 run_spectro_webs.py 2021-01-01 2021-01-02 G.CCD.00.BHZ```

## Output
The program outputs a file per stream and per day in a Figures directory.  
![Alt text](./images/G.CCD.00.BHZ.2021.001.png?raw=true)

