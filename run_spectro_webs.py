#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
:author:
   Dimitri Zigone (zigone@unistra.fr)
   Sophie Lamnbotte (sophie.lambotte@unistra.fr)
   Maxime Bes de Berc (mbesdeberc@unistra.fr)

:copyright:
   Dimitri Zigone (zigone@unistra.fr)
   Sophie Lamnbotte (sophie.lambotte@unistra.fr)
   Maxime Bes de Berc (mbesdeberc@unistra.fr)

:license:
    The Beerware License
    (https://tldrlegal.com/license/beerware-license)

Usage:
    run_spectro_webs.py fdsn <start> <end> <stream> [--url=<url>] \
[--hf-scale=<hfs>] [--lf-scale=<lfs>]

Example:
    run_spectro_webs.py fdsn 2016-09-30 2016-10-04 G.CCD.00.BHZ

Options:
    -h --help           Show this screen.
    --version           Show version.
    <start>             Start time compatible w. obspy.core.UTCDateTime. Fdsn \
mode only.
    <end>               End time compatible w. obspy.core.UTCDateTime. Fdsn \
mode only.
    <stream>            Stream to analyse in seed code separated by dots. \
Fdsn mode only. Multiple streams are allowed, separated by comma \
(Ex: G.CCD.?0.BH?,G.DRV.00.BH?).
    --url=<url>         Set base url of fdsnws server. Fdsn mode only \
[default: http://10.0.1.36:8080]
    --hf-scale=<hfs>    Set scale of colorbar for high frequencies \
spectrograms. [default: -200,-140]
    --lf-scale=<lfs>    Set scale of colorbar for low franquencies \
spectrograms. [default: -200,-140]
"""
from docopt import docopt
from multiprocessing import Pool
import numpy as np
from obspy import Stream
from obspy.clients.fdsn import Client
from obspy.clients.fdsn.header import FDSNNoDataException
import os

from spectro import create_custom_colormap, roundday, spectro


def __process_single_day_fdsn(_streams, _client, _time, _hfscale, _lfscale,
                              _colmap):
    st = Stream()
    for stream in _streams:
        code = stream.split('.')
        try:
            print("[{}.{:04}.{:03}] Downloading".format(stream,
                                                        _time.year,
                                                        _time.julday))
            st += _client.get_waveforms(code[0], code[1], code[2], code[3],
                                        _time-60, _time+86460,
                                        attach_response=True)
            st.trim(_time, _time+86400)
            st.merge(method=1, interpolation_samples=-1,
                     fill_value='interpolate')
            st.detrend('demean')
            st.detrend('linear')
            st.remove_response()
        except FDSNNoDataException:
            print("[{}.{:04}.{:03}] No data available".format(stream,
                                                              _time.year,
                                                              _time.julday))
        for tr in st:
            print("[{}.{:04}.{:03}] Processing".format(tr.id, _time.year,
                                                       _time.julday))
            spectro(tr, _hfscale, _lfscale, _colmap)


if __name__ == '__main__':
    args = docopt(__doc__, version='run_spectro_webs 0.1')
    # Uncomment for debug
    # print(args)

    colmap = create_custom_colormap()
    hfscale = np.array([int(x) for x in args['--hf-scale'].split(',')])
    lfscale = np.array([int(x) for x in args['--lf-scale'].split(',')])

    pool = Pool(os.cpu_count()-1)

    if args['fdsn']:
        client = Client(args['--url'])
        currentday = roundday(args['<start>'])
        endday = roundday(args['<end>'])
        while currentday < endday:
            pool.apply_async(__process_single_day_fdsn,
                             args=(args['<stream>'].split(','), client,
                                   currentday, hfscale, lfscale, colmap))
            currentday += 86400
    pool.close()
    pool.join()
